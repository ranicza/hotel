package java;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;

import org.dbunit.DatabaseTestCase;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.junit.Test;
import com.epam.te.hotel.model.dao.sql.UserDAO;
import com.epam.te.hotel.model.entity.User;
import com.epam.te.hotel.model.dao.DAOException;
import com.epam.te.hotel.model.dao.IUserDAO;

public class UserDAOTest  extends DatabaseTestCase{
	
    private static final String TESTDATA_FILE = "UserDAOTest-dataset.xml";

    public UserDAOTest(String orderDAOTest) {
            super(orderDAOTest);
    }
	
	@Test
	public void testFindUserByLogin(){
		IUserDAO userDAO = new UserDAO();
		try{
			User user = userDAO.findUserByLogin("admin");
			 assertNotNull(user);
		     assertEquals("admin", user.getLogin());
		     assertEquals(1, user.getIdRole());
		}catch (DAOException e){
			
		}

		
	}

    /** Returns the Connection object for DbUnit to use.
     * @see org.dbunit.DatabaseTestCase#getConnection()
     * dbUnit uses this method to obtain a connection to the database which
    // it is supposed to set up as a sandbox for the actual test methods
     */
	@Override
	protected IDatabaseConnection getConnection() throws Exception {
		Class driverClass = Class.forName("com.mysql.jdbc.Driver");
        String url = "jdbc:mysql://localhost/hotel_db";
        String usr = "root";
        String pwd = "root";
        Connection jdbcConnection = DriverManager.getConnection(url, usr, pwd);
        return new DatabaseConnection(jdbcConnection);
	}

    /**
     *  dbUnit uses this method to obtain the set of data that needs to be
     */
    // inserted into the database to set up the sandbox
	@Override
	protected IDataSet getDataSet() throws Exception {
		return new FlatXmlDataSet(new FileInputStream(TESTDATA_FILE));
	}
	
}
